/*
 * Copyright (c) 2018 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.reference.joicalculator;

import com.github.elwinbran.javaoverhaul.functional.Function;

/**
 *
 * @author Elwin Slokker
 */
public class BigEndianNumberAdderImplementation implements TripleByteArrayCombination
{

    private final Function<LoopIteration<byte[]>, byte[]> loopMachine;
    
    public BigEndianNumberAdderImplementation(Function<LoopIteration<byte[]>, byte[]> loopMachine)
    {
        this.loopMachine = loopMachine;
    }
    
    @Override
    public byte[] combination(byte[] firstOperand, byte[] secondOperand, byte[] thirdOperand)
    {
        //loop through all bytes and assign to each the correct answer based on byte sum
        LoopIteration<byte[]> firstIteration;//create from operands 
        firstIteration = new LoopIteration()
        {
            private int firstIndex = firstOperand.length - 1;
            
            private int secondIndex = secondOperand.length - 1;
            
            private int resultIndex = thirdOperand.length - 1;
            
            private short carry = 0;
            
            static final short LIMIT = (short)0b0000000011111111;
            
            @Override
            public IterationResult<byte[]> iterationResult()
            {
                final short first;
                if(this.firstIndex < 0)
                {
                    first = 0;
                }
                else
                {
                    first = (short)(firstOperand[this.firstIndex] & 0xFF);
                    this.firstIndex--;
                }
                final short second;
                if(this.secondIndex < 0)
                {
                    second = 0;
                }
                else
                {
                    second = (short)(secondOperand[this.secondIndex] & 0xFF);
                    this.secondIndex--;
                }
                final short temp = (short)(first + second + this.carry);
                thirdOperand[this.resultIndex] = (byte)(temp & LIMIT);
                this.resultIndex--;
                this.carry = (short)(temp >>> 8);
                return new IterationResult<byte[]>()
                {
                    @Override
                    public Boolean done()
                    {
                        if(firstIndex < 0)
                        {
                            if(secondIndex < 0)
                            {
                                return true;
                            }
                        }
                        return false;
                    }

                    @Override
                    public byte[] result()
                    {
                        return thirdOperand;
                    }
                };
            }

            @Override
            public LoopIteration<byte[]> nextIteration(Object currentValue)
            {
                return this;
            }
        
        };
        byte[] result = this.loopMachine.apply(firstIteration);
        return result;
    }
    
}
