/*
 * Copyright (c) 2018 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.reference.joicalculator;

import com.github.elwinbran.javaoverhaul.base.Executor;
import java.util.Iterator;
import javafx.beans.property.ReadOnlyProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 *
 * @author Elwin Slokker
 */
public class ExecutorIteration implements LoopIteration<Void>
{

    private final Iterator<Executor> executorSequence;
            
    private final Branch<ReadOnlyProperty<IterationResult<Void>>> picker;
    
    private final IterationResult<Void> finalResult;
    
    private final Boolean joiFalse;
    
    public ExecutorIteration(Iterator<Executor> executorSequence,
            Branch<ReadOnlyProperty<IterationResult<Void>>> picker,
            IterationResult<Void> finalResult, Boolean joiFalse)
    {
        this.executorSequence = executorSequence;
        this.picker = picker;
        this.finalResult = finalResult;
        this.joiFalse = joiFalse;
    }
    
    @Override
    public IterationResult<Void> iterationResult()
    {
        Boolean hasNext = executorSequence.hasNext();
        ReadOnlyProperty<IterationResult<Void>> normalIterationResult = 
                new SimpleObjectProperty()
                {
                    @Override
                    public IterationResult<Void> get()
                    {
                        return new IterationResult()
                        {
                            @Override
                            public Boolean done()
                            {
                                return joiFalse;
                            }

                            @Override
                            public Void result()
                            {
                                executorSequence.next().execute();
                                return null;
                            }
                    
                        };
                    }
                };
        ReadOnlyProperty<IterationResult<Void>> finalIterationResult =
                new SimpleObjectProperty()
                {
                    @Override
                    public IterationResult<Void> get()
                    {
                        return finalResult;
                    }
                };
        return this.picker.result(hasNext, normalIterationResult, finalIterationResult).getValue();
    }

    @Override
    public LoopIteration<Void> nextIteration(Void currentValue)
    {
        return this;
    }
    
}
