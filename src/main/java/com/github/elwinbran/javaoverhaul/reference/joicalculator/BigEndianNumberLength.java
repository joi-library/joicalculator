/*
 * Copyright (c) 2018 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.reference.joicalculator;

import com.github.elwinbran.javaoverhaul.functional.Function;
import com.github.elwinbran.javaoverhaul.functional.Operator;
import com.github.elwinbran.javaoverhaul.numbers.PositiveNumber;

/**
 *
 * @author Elwin Slokker
 */
public class BigEndianNumberLength implements Function<byte[], PositiveNumber>
{

    private final Function<byte[], Integer> arrayLenghtCounter;
    
    private final Function<byte[], Integer> leadingZeroCounter;
    
    private final Operator<Integer, Integer, Long> specializedSubraction;
    
    private final Function<Long, PositiveNumber> longUpgrader;
    
    public BigEndianNumberLength(Function<byte[], Integer> arrayLenghtCounter, 
            Function<byte[], Integer> leadingZeroCounter, 
            Operator<Integer, Integer, Long> specializedSubraction, 
            Function<Long, PositiveNumber> longUpgrader)
    {
        this.arrayLenghtCounter = arrayLenghtCounter;
        this.leadingZeroCounter = leadingZeroCounter;
        this.specializedSubraction = specializedSubraction;
        this.longUpgrader = longUpgrader;
    }

    @Override
    public PositiveNumber apply(byte[] argument)
    {
        Integer fullLength = this.arrayLenghtCounter.apply(argument);
        Integer leadingZeroBytes = this.leadingZeroCounter.apply(argument);
        Long tempResult = this.specializedSubraction.process(fullLength, leadingZeroBytes);
        PositiveNumber result = longUpgrader.apply(tempResult);
        return result;
    }
    
}
