/*
 * Copyright (c) 2018 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.reference.joicalculator;

import com.github.elwinbran.javaoverhaul.base.Comparator;
import com.github.elwinbran.javaoverhaul.functional.Operator;

/**
 *
 * @author Elwin Slokker
 */
public class BiggestInteger implements Operator<Integer, Integer, Integer>
{

    private final Branch<Integer> picker;
    private final Comparator<Integer> comparator;
    
    public BiggestInteger(Branch<Integer> branch, Comparator<Integer> javaIntComparator)
    {
        this.picker = branch;
        this.comparator = javaIntComparator;
    }

    @Override
    public Integer process(Integer firstOperand, Integer secondOperand)
    {
        Boolean choice = this.comparator.isGreater(firstOperand, secondOperand);
        Integer result = this.picker.result(choice, firstOperand, secondOperand);
        return result;
    }
    
}
