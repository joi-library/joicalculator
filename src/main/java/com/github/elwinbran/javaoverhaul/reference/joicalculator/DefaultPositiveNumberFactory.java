/*
 * Copyright (c) 2018 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.reference.joicalculator;

import com.github.elwinbran.javaoverhaul.base.Bytes;
import com.github.elwinbran.javaoverhaul.numbers.PositiveNumber;
import com.github.elwinbran.javaoverhaul.numbers.PositiveNumberFactory;

/**
 *
 * @author Elwin Slokker
 */
public class DefaultPositiveNumberFactory implements PositiveNumberFactory
{

    private final PositiveNumber zero;
    private final BytesFactory dataMaker;
    
    public DefaultPositiveNumberFactory(PositiveNumber zero, BytesFactory dataMaker)
    {
        this.zero = zero;
        this.dataMaker = dataMaker;
    }
    
    @Override
    public PositiveNumber zero()
    {
        return this.zero;
    }

    @Override
    public PositiveNumber make(byte[] number)
    {
        Bytes data = this.dataMaker.make(number);
        return make(data);
    }

    @Override
    public PositiveNumber make(Iterable<Byte> number)
    {
        Bytes data = this.dataMaker.make(number);
        return make(data);
    }
    
    private PositiveNumber make(Bytes data)
    {
        return () -> {return data;};
    }
    
}
