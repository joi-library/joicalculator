/*
 * Copyright (c) 2018 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.reference.joicalculator;

import com.github.elwinbran.javaoverhaul.functional.Operator;

/**
 * This operator takes any two {@link java.lang.Byte Bytes} and adds them into 
 * a {@link java.lang.Short} as if they were unsigned.
 * 
 * @author Elwin Slokker
 */
public class JavaUnsignedByteAdder implements Operator<Byte, Byte, Short>
{

    private final UnsignedByteToShort converter;
    
    private final Operator<Short, Short, Short> shortAdder;
    
    /**
     * 
     * @param converter
     * @param shortAdder An operator that adds two shorts together.
     */
    public JavaUnsignedByteAdder(UnsignedByteToShort converter,
            Operator<Short, Short, Short> shortAdder)
    {
        this.converter = converter;
        this.shortAdder = shortAdder;
    }
    
    @Override
    public Short process(Byte firstOperand, Byte secondOperand)
    {
        Short firstShort = converter.fromByte(firstOperand);
        Short secondShort = converter.fromByte(secondOperand);
        Short result = shortAdder.process(firstShort, secondShort);
        return result;
    }
    
}
