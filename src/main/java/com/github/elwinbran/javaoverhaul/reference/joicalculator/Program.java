/*
 * Copyright (c) 2018 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.reference.joicalculator;

import com.github.elwinbran.javaoverhaul.base.Bytes;
import com.github.elwinbran.javaoverhaul.base.Comparator;
import com.github.elwinbran.javaoverhaul.base.Equivalator;
import com.github.elwinbran.javaoverhaul.base.Executor;
import com.github.elwinbran.javaoverhaul.base.Traversable;
import com.github.elwinbran.javaoverhaul.base.NullableWrapper;
import com.github.elwinbran.javaoverhaul.base.NullableWrapperFactory;
import com.github.elwinbran.javaoverhaul.numbers.PositiveNumber;
import com.github.elwinbran.javaoverhaul.strings.String;
import com.github.elwinbran.javaoverhaul.strings.Character;
import com.github.elwinbran.javaoverhaul.functional.Function;
import com.github.elwinbran.javaoverhaul.functional.Operator;
import com.github.elwinbran.javaoverhaul.numbers.PositiveNumberFactory;
import com.github.elwinbran.javaoverhaul.strings.CharacterFormat;


import java.io.InputStream;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;

/**
 * This is a class that serves only to compose the calculator program.
 * The calculator is a command line based application that can add,
 * subtract, multiply and divide given integer numbers.
 * 
 * @author Elwin Slokker
 */
public class Program
{
    /**
     * 
     * Note: read from bottom to top for best understanding.
     * @param args
     */
    public static void main(java.lang.String[] args)
    {
        System.out.println("Creating program...");
        //Java abstractions
        byte javaZero = 0;
        byte javaOne = 1;
        byte javaTwo = 2;
        Function<Boolean, Boolean> booleanInverter = new Inverse();
        PrimitiveCastUtility pcu = new PrimitiveCastUtility();
        Function<Byte, Short> byteToShortConverter = new ByteToShort(pcu);
        Function<Short, Byte> shortToByteConverter = new ShortToByte(pcu);
        Function<Short, Integer> shortToIntConverter = new ShortToInt(pcu);
        Function<Integer, Short> intToShortConverter = new IntToShort(pcu);
        Function<Integer, Long> intToLongConverter = new IntToLong(pcu);
        Function<Long, Integer> longToIntConverter = new LongToInt(pcu);
        Function<Long, Long> javaNegative = new JavaNegation();
        Function<byte[], Integer> arrayLenghtCounter = new JavaArrayLengthCount();
        JavaWholeNumberOperator javaAdder = new JavaAddition();
        JavaWholeNumberOperator javaSubtracter = new JavaSubtraction();
        JavaWholeNumberOperator javaBinaryAND = new JavaAND();
        JavaWholeNumberOperator javaBinaryOR = new JavaOR();
        JavaWholeNumberOperator javaLeftShifter = new JavaLeftShift();
        JavaWholeNumberOperator javaRightShifter = new JavaRightShift();
        JavaWholeNumberOperator javaRightZeroFillShifter = new JavaRightZeroFillShift();
        Function<byte[], Integer> javaDataLengthCounter = (byte[] input) ->
        {return input.length;};
        Comparator<Byte> javaByteComparator = (Byte left, Byte right) ->
        {return left > right;};
        Comparator<Integer> javaIntComparator = (Integer left, Integer right) ->
        {return left > right;};
        //TODO wrappers for Java specific classes.
        Branch branchFunction = new JavaIf();
        Operator<Integer, Integer, Integer> biggestIntPicker = 
                new BiggestInteger((Branch<Integer>) branchFunction, javaIntComparator);
        Function<LoopIteration, ?> loopFunction = new JavaWhile(booleanInverter);
        
        //JOI
        Equivalator<Boolean> booleanChecker = new EqualBoolean();
        Boolean joiFalse = Boolean.FALSE;
        Boolean joiTrue = Boolean.TRUE;
        Operator<Boolean, Boolean, Boolean> logicalAND = new LogicalAND();
        Operator<Boolean, Boolean, Boolean> logicalOR = new LogicalOR();
        Operator<Boolean, Boolean, Boolean> logicalXOR = new LogicalXOR();
        Operator<Boolean, Boolean, Boolean> logicalXNOR = 
                new LogicalXNOR(logicalXOR, booleanInverter);
        Equivalator<Iterable<Boolean>> sequenceChecker = new BooleanSequenceEquivalator();
        NullableWrapper emptyWrapper = null;
        NullableWrapperFactory nullableMaker = 
                new DefaultNullableWrapperFactory(joiFalse, emptyWrapper);
        Iterable emptyIterable = () -> {return new Iterator()
        {
            @Override
            public boolean hasNext()
            {
                return joiFalse;
            }

            @Override
            public Object next()
            {
                return null;
            }
            
        };};
        Traversable emptySequence = null;
        PositiveNumber zero = null;
        Comparator<PositiveNumber> numberComparator = null;
        
        //Operator<PositiveNumber, PositiveNumber, PositiveNumber> biggestNumberPicker;
        //biggestNumberPicker = new BiggestNumber(numberComparator, branchFunction);
        
        Operator<Byte, Byte, Short> javaByteAdder = 
                new JavaUnsignedByteAdder(null, null);
        BytesCreator bytesCreator = (byte[] data, Iterable<Byte> objectRepresentation) ->
        {return new DefaultBytes(data, objectRepresentation);};
        BytesFactory dataMaker = new DefaultBytesFactory(bytesCreator);
        PositiveNumberFactory positiveMaker = 
                new DefaultPositiveNumberFactory(zero, dataMaker);
        Function<Byte, Bytes> binaryExpander = (Byte data) -> {
            return new Bytes(){
                @Override
                public byte[] data()
                {
                    return new byte[]{data};
                }

                @Override
                public Iterable<Byte> objectRepresentation()
                {
                    return null;
                }
                
            };
        };
        Function<Long, PositiveNumber> longUpgrader = null;
        Function<byte[], Integer> leadingZeroCounter = null;
        Operator<Integer, Integer, Long> specializedSubraction = new IntegerBasicSubtraction(intToLongConverter, javaSubtracter);
        Function<PositiveNumber, Long> numberDegrader = new PositiveDegrader(javaLeftShifter);
        //Function<byte[], PositiveNumber> bigEndianLengthCounter = 
        //        new BigEndianNumberLength(arrayLenghtCounter, leadingZeroCounter,
        //                specializedSubraction, longUpgrader);
        Function<Byte, Long> byteToLongConverter = 
                new ByteToLong(byteToShortConverter, shortToIntConverter, intToLongConverter);
                
        
        Function<LoopIteration<byte[]>, byte[]> binLoop = new JavaWhile<>(booleanInverter);
        TripleByteArrayCombination tripleAdderCombiner = new BigEndianNumberAdderImplementation(binLoop);
        Operator<byte[], byte[], byte[]> bigEndianNumberAdder = new BigEndianNumberAdder(biggestIntPicker, tripleAdderCombiner, javaDataLengthCounter);
        Operator<PositiveNumber, PositiveNumber, PositiveNumber> positiveAdder = 
                new PositiveNumberAddition(positiveMaker, bigEndianNumberAdder);
        String emptyString = new DefaultString(emptyIterable);
        String defaultFormatName = null;
        CharacterFormat defaultFormat = () -> defaultFormatName;
        
        
        
        /*
        Function<byte[], Bytes> binarySequenceExpander = null;
        Combiner<Character, java.lang.String> fromCharactersToNativeString = null;
        Function<String, java.lang.String> toNativeStringConverter = 
        (String argument) -> {
            Iterable<Character> characters = argument.characterSequence();
            return fromCharactersToNativeString.combine(characters);
        };
        Combiner<java.lang.Character, String> fromNativeCharactersToString = null;
        Function<char[], Traversable<java.lang.Character>> charArrayToTraversable = null;
        Function<java.lang.String, String> fromNativeStringConverter = 
                (java.lang.String argument) -> {
                    char[] chars = argument.toCharArray();
                    Traversable<java.lang.Character> objectifiedCharacters = 
                    charArrayToTraversable.apply(chars);
                    return fromNativeCharactersToString.combine(objectifiedCharacters);
        };
        */
        
        //Exception systems
        /*
        ActionGroup unknownAction = null;
        ActionGroup programmerFault = null;//"Programmer intervention required."
        ExceptionTypeFactory exceptionTypeMaker = new DefaultExceptionTypeFactory();
        ExceptionType unknownException = exceptionTypeMaker.makeType(null, unknownAction, emptyWrapper);
        NullableWrapper<ExceptionType> wrappedUnknownException = nullableMaker.make(unknownException);
        ExceptionType reserved = exceptionTypeMaker.makeType(
                null, unknownAction, wrappedUnknownException);
        */
        //...
        //DirtyConverter.fromBytes(botchedNumber);
        
        //somthing that creates full Bytes from byte[]
        Function<java.lang.String, PositiveNumber> highInputConverter;
        highInputConverter = (java.lang.String input) ->
        {
            byte[] numberData = DirtyConverter.fromString(input);
            return positiveMaker.make(numberData);
        };
        Function<PositiveNumber, java.lang.String> highOutputConverter;
        highOutputConverter = (PositiveNumber input) ->
        {
            byte[] numberData = input.binaryRepresentation().data();
            return DirtyConverter.fromBytes(numberData);
        };
        
        Operator<java.lang.String, java.lang.String, java.lang.String> highLevelAdder;
        highLevelAdder = (java.lang.String firstOperand, java.lang.String secondOperand) ->
        {
            PositiveNumber firstNumber = highInputConverter.apply(firstOperand);
            PositiveNumber secondNumber = highInputConverter.apply(secondOperand);
            PositiveNumber result = positiveAdder.process(firstNumber, secondNumber);
            return highOutputConverter.apply(result);
        };
        
        Property<java.lang.String> firstOperand = new SimpleStringProperty();
        Property<java.lang.String> secondOperand = new SimpleStringProperty();
        
        InputStream systemInput = System.in;
        Scanner commandInput = new Scanner(systemInput);
        java.lang.String initialPromptText = "Type first operand (a number):";
        java.lang.String secondPromptText = "Type second operand (a number):";
        java.lang.String finalText = "Result of operation: ";
        Property<java.lang.String> resultProperty = new SimpleStringProperty()
        {
            @Override
            public java.lang.String getValue()
            {
                //use operand into the calculator here.
                java.lang.String result =
                        highLevelAdder.process(firstOperand.getValue(), 
                                               secondOperand.getValue());
                return finalText + result;
            }
        };
        //string, string, string -> string
        Executor printer1 = new Print(initialPromptText);
        Executor input1 = new Input(commandInput, firstOperand);
        Executor printer2 = new Print(secondPromptText);
        Executor input2 = new Input(commandInput, secondOperand);
        Executor printer3 = new LazyPrint(resultProperty);
        Iterable<Executor> programSequence = 
                Arrays.asList(printer1, input1, printer2, input2, printer3);
        //Something that captures the statement order...
        Function<LoopIteration<Void>, Void> executor = new JavaWhile(booleanInverter);
        IterationResult<Void> finalResult = new DefaultIterationResult(null, joiTrue);
        LoopIteration<Void> programStart = 
                new ExecutorIteration(programSequence.iterator(), branchFunction, finalResult, joiFalse);
        
        Executor calculatorProgram = new SequenceExecutor(programStart, executor);
        System.out.println("Program start");
        calculatorProgram.execute();
        System.out.println("Program end");
    }
    
    private static class SequenceExecutor implements Executor
    {

        private final LoopIteration<Void> statements;
        
        private final Function<LoopIteration<Void>, Void> executor;
        
        public SequenceExecutor(LoopIteration<Void> statements, 
                                Function<LoopIteration<Void>, Void> executor)
        {
            this.statements = statements;
            this.executor = executor;
        }
        
        @Override
        public void execute()
        {
            this.executor.apply(this.statements);
        }
        
    }
    
    private static class Print implements Executor
    {
        private final java.lang.String text;
        
        public Print(java.lang.String text)
        {
            this.text = text;
        }
        
        @Override
        public void execute()
        {
            System.out.println(text);
        }
    }
    
    private static class LazyPrint implements Executor
    {
        private final Property<java.lang.String> expression;
        
        public LazyPrint(Property<java.lang.String> expression)
        {
            this.expression = expression;
        }
        
        @Override
        public void execute()
        {
            java.lang.String output = expression.getValue();
            System.out.println(output);
        }
    }
    
    private static class Input implements Executor
    {
        private final Scanner commandInput;
        private final Property<java.lang.String> userInput;
        
        public Input(Scanner commandInput, Property<java.lang.String> userInput)
        {
            this.commandInput = commandInput;
            this.userInput = userInput;
        }

        @Override
        public void execute()
        {
            java.lang.String input = this.commandInput.next();
            this.userInput.setValue(input);
        }
    }
    
    private static interface IntegerCalculator
    {
        public PositiveNumber calculate(PositiveNumber first,
               OperatorSymbol operator, PositiveNumber Second);
    }
    
    private static interface OperatorSymbol
    {
        public Character symbol();
    }
    
    private static class Inverse implements Function<Boolean, Boolean>
    {
        @Override
        public Boolean apply(Boolean argument)
        {
            return !argument;
        }
    }
    
    private static class JavaIf<Output> implements Branch<Output>
    {
        @Override
        public Output result(Boolean choice, Output trueResult, Output falseResult)
        {
            Output result;
            if(choice)
            {
                result = trueResult;
            }
            else
            {
                result = falseResult;
            }
            return result;
        }
    }
    
    private static class BranchFunction<Output> implements Function<Boolean, Output>
    {
        private final Branch<Output> brancher;
        
        private final Output trueResult;
        
        private final Output falseResult;
        
        public BranchFunction(Branch<Output> brancher, Output trueResult,
                Output falseResult)
        {
            this.brancher = brancher;
            this.trueResult = trueResult;
            this.falseResult = falseResult;
        }
        
        @Override
        public Output apply(Boolean argument)
        {
            return this.brancher.result(argument, trueResult, falseResult);
        }
    }
    
    private static class JavaWhile<Output> implements Function<LoopIteration<Output>, Output>
    {

        private final Function<Boolean, Boolean> inverter;
        
        public JavaWhile(Function<Boolean, Boolean> inverter)
        {
            this.inverter = inverter;
        }
        
        @Override
        public Output apply(LoopIteration<Output> argument)
        {
            IterationResult<Output> intermediateResult = argument.iterationResult();
            Output result = intermediateResult.result();
            Boolean unfinished = inverter.apply(intermediateResult.done());
            while(unfinished)
            {
                argument = argument.nextIteration(result);
                intermediateResult  = argument.iterationResult();
                result = intermediateResult.result();
                unfinished = inverter.apply(intermediateResult.done());
            }
            return result;
        }
        
    }
}
