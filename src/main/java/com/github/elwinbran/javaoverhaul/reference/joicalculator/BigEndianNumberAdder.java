/*
 * Copyright (c) 2018 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.reference.joicalculator;

import com.github.elwinbran.javaoverhaul.functional.Function;
import com.github.elwinbran.javaoverhaul.functional.Operator;
import com.github.elwinbran.javaoverhaul.numbers.PositiveNumber;

/**
 *
 * @author Elwin Slokker
 */
public class BigEndianNumberAdder implements Operator<byte[], byte[], byte[]>
{

    //private final byte javaOne;
    private final Operator<Integer, Integer, Integer> biggestNumberPicker;
    private final TripleByteArrayCombination additionInserter;
    private final Function<byte[], Integer> lengthCounter;
    
    public BigEndianNumberAdder(Operator<Integer, Integer, Integer> biggestNumberPicker,
            TripleByteArrayCombination additionInserter,
            Function<byte[], Integer> lengthCounter)
    {
        this.biggestNumberPicker = biggestNumberPicker;
        this.additionInserter = additionInserter;
        this.lengthCounter = lengthCounter;
    }
    
    @Override
    public byte[] process(byte[] firstOperand, byte[] secondOperand)
    {
        /*
        byte zero;
        byte one;
        short byteFilled
        number eight? for shifting?
        
        */
        Function<Integer, Long> integerToLong = (Integer input) ->
        {return (Long)(long)(int)input;};
        Function<Long, Integer> longToInteger = (Long input) ->
        {return (Integer)(int)(long)input;};
        Function<Byte, Long> byteToLong = null;
        Integer firstLength = this.lengthCounter.apply(firstOperand);
        Integer secondLength = this.lengthCounter.apply(secondOperand);
        Integer biggestLength = this.biggestNumberPicker.process(firstLength, secondLength);
        JavaWholeNumberOperator adder = new JavaAddition();
        Long one = 1L;
        Long biggestLengthLong = integerToLong.apply(biggestLength);
        Long correctedLength = adder.result(biggestLengthLong, one);
        Integer usableLength = longToInteger.apply(correctedLength);
        
        //somthing that takes the bytes and produces a carry and the result in bytes.
        byte[] result = new byte[usableLength];
        //give the lengths as integer???
        result = this.additionInserter.combination(firstOperand, secondOperand, result);
        //newResult = f(result, firstOperand, secondOperand);
        //f auto creates some loop and executes it
        return result;
    }
    
}
