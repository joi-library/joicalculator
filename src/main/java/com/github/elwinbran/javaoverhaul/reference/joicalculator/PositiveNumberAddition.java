/*
 * Copyright (c) 2018 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.reference.joicalculator;

import com.github.elwinbran.javaoverhaul.functional.Operator;
import com.github.elwinbran.javaoverhaul.numbers.PositiveNumber;
import com.github.elwinbran.javaoverhaul.numbers.PositiveNumberFactory;

/**
 *
 * @author Elwin Slokker
 */
public class PositiveNumberAddition implements Operator<PositiveNumber, PositiveNumber, PositiveNumber>
{

    private final Operator<byte[], byte[], byte[]> bigEndianNumberAdder;
    private final PositiveNumberFactory numberMaker;
    
    public PositiveNumberAddition(PositiveNumberFactory numberMaker,
            Operator<byte[], byte[], byte[]> bigEndianNumberAdder)
    {
        this.bigEndianNumberAdder = bigEndianNumberAdder;
        this.numberMaker = numberMaker;
    }
    
    @Override
    public PositiveNumber process(PositiveNumber firstOperand, PositiveNumber secondOperand)
    {
        byte[] firstNumber = firstOperand.binaryRepresentation().data();
        byte[] secondNumber = secondOperand.binaryRepresentation().data();
        byte[] dataResult = this.bigEndianNumberAdder.process(firstNumber, secondNumber);
        PositiveNumber result = this.numberMaker.make(dataResult);
        return result;
    }
    
}
