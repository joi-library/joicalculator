/*
 * Copyright (c) 2018 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.reference.joicalculator;

import com.github.elwinbran.javaoverhaul.functional.Function;


public class JavaUnsignedByteToShort implements UnsignedByteToShort
{
    private final JavaWholeNumberOperator javaLogicalAND;
    
    private final Function<Long, Short> converter;
    
    private final Function<Short, Long> toConverter;
    
    private final Function<Byte, Long> byteConverter;
    
    public JavaUnsignedByteToShort(JavaWholeNumberOperator javaLogicalAND, 
            Function<Long, Short> converter, Function<Short, Long> toConverter, 
            Function<Byte, Long> byteConverter)
    {
        this.javaLogicalAND = javaLogicalAND;
        this.converter = converter;
        this.toConverter = toConverter;
        this.byteConverter = byteConverter;
    }
    
    @Override
    public Short fromByte(Byte unsignedByteNumber)
    {
        short limit = (short)0b0000000011111111;
        Long longerLimit = toConverter.apply(limit);
        Long number = byteConverter.apply(unsignedByteNumber);
        Long logicalResult = javaLogicalAND.result(longerLimit, number);
        Short result = this.converter.apply(logicalResult);
        return result;
    }
}
