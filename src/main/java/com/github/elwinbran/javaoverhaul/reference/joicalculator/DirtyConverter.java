/*
 * Copyright (c) 2018 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.reference.joicalculator;

import java.math.BigInteger;

/**
 * A simple converter not meant for enterprise applications that converts Java
 * Strings into arrays of bytes where every byte should be treated as unsigned.
 * 
 * @author Elwin Slokker
 */
public class DirtyConverter
{
    public static byte[] fromString(String number)
    {
        double unroundedBinaryDigits = (10d/3d) * (double)number.length();
        double requiredBinaryDigits = (int) Math.ceil(unroundedBinaryDigits);
        int requiredByteAmount = (int) Math.ceil(requiredBinaryDigits / 8d);
        byte[] convertedNumber = new byte[requiredByteAmount];
        for(Character character : number.toCharArray())
        {
            switch(character)
            {
                case '0': break;
                case '1': break;
                case '2': break;
                case '3': break;
                case '4': break;
                case '5': break;
                case '6': break;
                case '7': break;
                case '8': break;
                case '9': break;
                default: throw new RuntimeException(
                        "Input contained non-numeric characters. Only '0'-'9' are accepted."); 
            }
        }
        BigInteger workableNumber = new BigInteger(number, 10);
        String binaryRepresentation = workableNumber.toString(2);
        int arrayCursor = convertedNumber.length - 1;
        int bytePosition = 0;
        byte currentByte = 0;
        for(int i = binaryRepresentation.toCharArray().length - 1; i >= 0; i--)
        {
            Character character = binaryRepresentation.toCharArray()[i];
            if(!character.equals('0'))
            {
                if(bytePosition < 7)
                {
                    currentByte += Math.pow(2, bytePosition);
                }
                else
                {
                    currentByte -= Math.pow(2, 7);
                }
            }
            bytePosition++;
            if(bytePosition == 8)
            {
                bytePosition = 0;
                convertedNumber[arrayCursor] = currentByte;
                arrayCursor--;
                currentByte = 0;
            }
            else if(i == 0)
            {
                convertedNumber[arrayCursor] = currentByte;
            }
        }
        return convertedNumber;
    }
    
    public static String fromBytes(byte[] botchedNumber)
    {
        String looky = "";
        for(Byte number : botchedNumber)
        {
            looky = looky.concat(String.format("%8s", Integer.toBinaryString(number & 0xFF)).replace(' ', '0'));
        }
        BigInteger converted = new BigInteger(looky, 2);
        return converted.toString(10);
    }
}
