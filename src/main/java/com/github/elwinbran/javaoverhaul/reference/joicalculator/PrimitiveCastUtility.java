/*
 * Copyright (c) 2018 Elwin Slokker
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.github.elwinbran.javaoverhaul.reference.joicalculator;

/**
 * A class that centralizes the java primitive conversions.
 * These are operations categorized as casts, but in my opinion casts exist for
 * Objects only.
 * 
 * @author Elwin Slokker
 */
public final class PrimitiveCastUtility
{
    public short byteToShort(byte argument)
    {
        return (short)argument;
    }
    
    public byte shortToByte(short argument)
    {
        return (byte)argument;
    }
    
    public int shortToInt(short argument)
    {
        return (int)argument;
    }
    
    public short intToShort(int argument)
    {
        return (short)argument;
    }
    
    public long intToLong(int argument)
    {
        return (long)argument;
    }
    
    public int longToInt(long argument)
    {
        return (int)argument;
    }
}
